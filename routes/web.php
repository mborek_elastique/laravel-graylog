<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => '\Wocozon\Logging\Http\Controllers',
    'middleware' => ['web', 'auth:web'],
    'prefix' => 'logging',
], static function (): void {
    Route::get('test', 'TestController@index');
});
