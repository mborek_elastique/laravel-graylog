<?php

namespace Wocozon\Logging;

use Gelf\Publisher;
use Gelf\PublisherInterface;
use Gelf\Transport\TcpTransport;
use Illuminate\Support\ServiceProvider;
use Monolog\Formatter\GelfMessageFormatter;
use Monolog\Handler\GelfHandler;
use Wocozon\Logging\Taps\EnrichLoggerTap;

class GraylogServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes(
            [
                \dirname(__DIR__).'/config/graylog.php' => \config_path('graylog.php'),
            ]
        );

        $this->loadRoutesFrom(\dirname(__DIR__).'/routes/web.php');
    }

    public function register(): void
    {
        $this->mergeConfigFrom(
            \dirname(__DIR__).'/config/graylog.php',
            'graylog'
        );

        $this->app->singleton(
            PublisherInterface::class, static function ($app) {
            $transport = new TcpTransport($app['config']['graylog']['host'], $app['config']['graylog']['port']);
            return new Publisher($transport);
        });

        if (!\is_array(\config('logging.channels.graylog', null))) {
            \config(
                [
                    'logging.channels.graylog' => [
                        'driver' => 'monolog',
                        'handler' => GelfHandler::class,
                        'formatter' => GelfMessageFormatter::class,
                        'formatter_with' => [
                            'extraPrefix' => 'extra_',
                            'contextPrefix' => 'context_',
                        ],
                        'tap' => [
                            EnrichLoggerTap::class,
                        ]
                    ],
                ]
            );
        }
    }
}
