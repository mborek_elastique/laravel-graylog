<?php

namespace Wocozon\Logging\Taps;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Monolog\Handler\GelfHandler;
use Monolog\Processor\GitProcessor;
use Monolog\Processor\HostnameProcessor;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\UidProcessor;
use Monolog\Processor\WebProcessor;
use Wocozon\Logging\Processors\EnvironmentContextProcessor;
use Wocozon\Logging\Processors\ExceptionContextProcessor;
use Wocozon\Logging\Processors\UserContextProcessor;

class EnrichLoggerTap
{
    protected Application $application;
    protected ?Request $request = null;

    public function __construct(Application $application, ?Request $request = null)
    {
        $this->application = $application;
        $this->request = $request;
    }

    /**
     * @param Logger|\Monolog\Logger $logger
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof GelfHandler) {
                if ($this->request !== null && $this->application->runningInConsole() === false) {
                    $handler
                        ->pushProcessor(new UserContextProcessor($this->request))
                        ->pushProcessor(
                            new WebProcessor(
                                $this->request->server(),
                                [
                                    'http_method' => 'REQUEST_METHOD',
                                    'ip' => 'HTTP_X_FORWARDED_FOR',
                                    'referrer' => 'HTTP_REFERER',
                                    'server' => 'SERVER_NAME',
                                    'url' => 'REQUEST_URI',
                                ])
                        )
                    ;
                }

                $handler
                    ->pushProcessor(new EnvironmentContextProcessor($this->application, $logger->getName()))
                    ->pushProcessor(new ExceptionContextProcessor())
                    ->pushProcessor(new MemoryPeakUsageProcessor(true, false))
                    ->pushProcessor(new MemoryUsageProcessor(true, false))
                    ->pushProcessor(new GitProcessor())
                    ->pushProcessor(new HostnameProcessor())
                    ->pushProcessor(new ProcessIdProcessor())
                    ->pushProcessor(new UidProcessor())
                ;
            }
        }
    }
}
