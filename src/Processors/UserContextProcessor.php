<?php

namespace Wocozon\Logging\Processors;

use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

class UserContextProcessor implements ProcessorInterface
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function __invoke(array $record)
    {
        if ($user = $this->request->user()) {
            $record['extra']['user_id'] = \optional($user)->id;
            $record['extra']['user_email'] = \optional($user)->email;
        }

        return $record;
    }
}
