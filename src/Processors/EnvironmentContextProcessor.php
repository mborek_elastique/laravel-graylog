<?php

namespace Wocozon\Logging\Processors;

use Illuminate\Contracts\Foundation\Application;
use Monolog\Processor\ProcessorInterface;

class EnvironmentContextProcessor implements ProcessorInterface
{
    protected Application $application;
    protected string $channel;

    public function __construct(Application $application, string $channel)
    {
        $this->application = $application;
        $this->channel = $channel;
    }

    public function __invoke(array $record)
    {
        $record['extra']['channel'] = $this->channel;
        $record['extra']['environment'] = $this->application->environment();
        $record['extra']['cli'] = $this->application->runningInConsole();
        $record['extra']['debug'] = $this->application->get('config')->get('app.debug') ?? false;
        return $record;
    }
}
