<?php

namespace Wocozon\Logging\Processors;

use Monolog\Processor\ProcessorInterface;

class ExceptionContextProcessor implements ProcessorInterface
{
    protected ?string $prefix = null;

    public function __construct(?string $prefix = 'EXCEPTION')
    {
        $this->prefix = $prefix;
    }

    public function __invoke(array $record)
    {
        if ($this->prefix && ($record['context']['exception'] ?? false)) {
            $record['message'] = "[{$this->prefix}]: {$record['message']}";
        }
        return $record;
    }
}
