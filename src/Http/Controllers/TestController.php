<?php

namespace Wocozon\Logging\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TestController extends Controller
{
    public function index(Request $request): Response
    {
        throw new \HttpException('Graylog test exception!', 418);
    }
}
