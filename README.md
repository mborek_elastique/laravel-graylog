# Laravel Graylog

Put in your `.env` file following variables:

    GRAYLOG_HOST=
    GRAYLOG_PORT=
    GRAYLOG_FACILITY=
    
Last one is optional...

Have fun with `graylog` logging channel in your app! ;-)
