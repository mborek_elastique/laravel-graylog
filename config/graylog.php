<?php

return [
    'host' => \env('GRAYLOG_HOST', '127.0.0.1'),
    'port' => \env('GRAYLOG_PORT', 12201),
    'facility' => \env('GRAYLOG_FACILITY') ?? \env('APP_ENV'),
];
